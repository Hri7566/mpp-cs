FROM gitpod/workspace-full

RUN sudo apt-get update \
    && sudo apt-get install -y \
        ... \
    && sudo rm -rf /var/lib/apt/lists/*

RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
    && sudo dpkg -i packages-microsoft-prod.deb

RUN sudo apt-get install -y apt-transport-https && \
    sudo apt-get update && \
    sudo apt-get install -y dotnet-sdk-5.0

RUN npm install
