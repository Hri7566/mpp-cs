var edge = require('edge-js');

const fs = require('fs');
const path = require('path');
const Client = require('mpp-client-xt');
var client = new Client('wss://mpp.hri7566.info:8443');
var csHandler = edge.func(path.join(__dirname, "Test.cs"));

client.on('hi', p => {
    console.log('Online on MPP');
})

client.start();
client.setChannel('lobby');

client.on('a', msg => {
    msg.args = msg.a.split(' ');
    msg.cmd = msg.args[0].toLowerCase().split('cs!').join('');
    csHandler(msg, (err, result) => {
        if (err) throw err;
        result !== '' && typeof result !== 'undefined' && result !== null ? client.sendArray([{m:'a', message: result}]) : 0;
    });
});

client.on('error', err => {
    client.start();
})
